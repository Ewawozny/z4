#pragma once

#ifndef bst_h
#define bst_h

class bst {
public:
    struct node {
        int data;
        node* left;
        node* right;
    };
    node* root;

    bst();
    ~bst();
    void insert(int value);
    node* insert(int, node*);
    void inorderH(node*);
    void preorderH(node*);
    void postorderH(node*);
    void inorder();
    void preorder();
    void postorder();
};

#endif //bst_h
