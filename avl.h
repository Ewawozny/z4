#pragma once

#ifndef avl_H
#define avl_H

class avl {
public:
    struct node {
        int data;
        node* left;
        node* right;
        int height;
    };

    node* root;

    avl();
    ~avl();
    node* insert(int, node*);
    node* findMin(node*);
    node* findMax(node*);
    node* remove(int, node*);
    int height(node*);
    int getBalance(node*);

    void insert(int);
    void remove(int);

    node *singleRightRotate(node *&t);
    node *singleLeftRotate(node *&t);
    node *doubleLeftRotate(node *&t);
    node *doubleRightRotate(node *&t);

    void inorderH(node*);
    void preorderH(node *x);
    void postorderH(node *x);

    void inorder();
    void preorder();
    void postorder();
};


#endif //avl_H
