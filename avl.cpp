#include <iostream>
#include "avl.h"

avl::~avl() = default;

avl::avl() {
    root = NULL;
}

avl::node* avl::insert(int x, node* t) {
    if(t == NULL) {
        t = new node;
        t->data = x;
        t->height = 0;
        t->left = t->right = NULL;
    } else if(x < t->data) {
        t->left = insert(x, t->left);
        if(height(t->left) - height(t->right) == 2) {
            if(x < t->left->data) t = singleRightRotate(t);
            else t = doubleRightRotate(t);
        }
    } else if(x > t->data) {
        t->right = insert(x, t->right);
        if(height(t->right) - height(t->left) == 2) {
            if(x > t->right->data) t = singleLeftRotate(t);
            else t = doubleLeftRotate(t);
        }
    }

    t->height = std::max(height(t->left), height(t->right))+1;
    return t;
}

avl::node* avl::singleRightRotate(node* &t) {
    node* u = t->left;
    t->left = u->right;
    u->right = t;
    t->height = std::max(height(t->left), height(t->right))+1;
    u->height = std::max(height(u->left), t->height)+1;
    return u;
}

avl::node* avl::singleLeftRotate(node* &t) {
    node* u = t->right;
    t->right = u->left;
    u->left = t;
    t->height = std::max(height(t->left), height(t->right))+1;
    u->height = std::max(height(t->right), t->height)+1 ;
    return u;
}

avl::node* avl::doubleLeftRotate(node* &t) {
    t->right = singleRightRotate(t->right);
    return singleLeftRotate(t);
}

avl::node* avl::doubleRightRotate(node* &t) {
    t->left = singleLeftRotate(t->left);
    return singleRightRotate(t);
}

avl::node* avl::findMin(node* t) {
    if(t == NULL) return NULL;
    else if(t->left == NULL) return t;
    else return findMin(t->left);
}

avl::node* avl::findMax(node* t) {
    if(t == NULL) return NULL;
    else if(t->right == NULL) return t;
    else return findMax(t->right);
}

avl::node* avl::remove(int x, node* t) {
    node* temp;
    if(t == NULL) return NULL;
    else if(x < t->data) t->left = remove(x, t->left);
    else if(x > t->data) t->right = remove(x, t->right);
    else if (t->left && t->right) {
        temp = findMin(t->right);
        t->data = temp->data;
        t->right = remove(t->data, t->right);
    } else {
        temp = t;
        if(t->left == NULL) t = t->right;
        else if(t->right == NULL) t = t->left;
        delete temp;
    }

    if(t == NULL) return t;

    t->height = std::max(height(t->left), height(t->right))+1;

    if (height(t->left) - height(t->right) == 2) {
        if(height(t->left->left) - height(t->left->right) == 1) return singleLeftRotate(t);
        else return doubleLeftRotate(t);
    }
    else if (height(t->right) - height(t->left) == 2) {
        if (height(t->right->right) - height(t->right->left) == 1) return singleRightRotate(t);
        else return doubleRightRotate(t);
    }
    return t;
}

int avl::height(node* x) {
    return (x == NULL ? -1 : x->height);
}

int avl::getBalance(node* t) {
    if(t == NULL) return 0;
    else return height(t->left) - height(t->right);
}

void avl::insert(int x) {
    root = insert(x, root);
}

void avl::remove(int x) {
    root = remove(x, root);
}

void avl::inorderH(node* x) {
    if(x == NULL) return;
    inorderH(x->left);
    std::cout<<x->data<<" ";
    inorderH(x->right);
}

void avl::inorder() {
    inorderH(root);
    std::cout << std::endl;
}

void avl::preorderH(node* x) {
    if (x == NULL) return;
    std::cout<<x->data<<" ";
    preorderH(x->left);
    preorderH(x->right);
}

void avl::preorder() {
    preorderH(root);
}

void avl::postorderH(node* x) {
    if (x == NULL) return;
    postorderH(x->left);
    postorderH(x->right);
    std::cout<<x->data<<" ";
}

void avl::postorder() {
    postorderH(root);
}
