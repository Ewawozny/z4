#include <iostream>
#include "bst.h"
#include "avl.h"

using namespace std;

int main() {

    bst root;
    root.insert(5);
    root.insert(2);
    root.insert(6);
    root.insert(8);
    root.insert(9);
    root.insert(34);
    root.insert(235);

    cout<<"BST inorder: "; root.inorder(); cout<<endl;
    cout<<"BST preorder: "; root.preorder(); cout<<endl;
    cout<<"BST postorder: "; root.postorder(); cout<<endl<<endl;

    avl t;
    t.insert(20);
    t.insert(25);
    t.insert(15);
    t.insert(10);
    t.insert(30);
    t.insert(5);
    t.insert(35);
    t.insert(67);
    t.insert(43);
    t.insert(21);
    t.insert(10);
    t.insert(89);
    t.insert(38);
    t.insert(69);
    cout<<"AVL inorder: "; t.preorder(); cout<<endl<<endl;

}