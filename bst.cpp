#include <iostream>
#include "bst.h"

bst::~bst() = default;

bst::bst() {
    root = NULL;
}

bst::node* bst::insert(int value, node* x) {
    if (x == NULL) {
        x = new node;
        x->data = value;
        x->left = x-> right = NULL;
        return x;
    };
    if (value > x->data) x->right = insert(value, x->right);
    else x->left = insert(value, x->left);
    return x;
}

void bst::insert(int value) {
    root = insert(value, root);
}

void bst::inorderH(node* x) {
    if (x == NULL) return;
    inorderH(x->left);
    std::cout<<x->data<<" ";
    inorderH(x->right);
}

void bst::inorder() {
    inorderH(root);
}

void bst::preorderH(node* x) {
    if (x == NULL) return;
    std::cout<<x->data<<" ";
    preorderH(x->left);
    preorderH(x->right);
}

void bst::preorder() {
    preorderH(root);
}

void bst::postorderH(node* x) {
    if (x == NULL) return;
    postorderH(x->left);
    postorderH(x->right);
    std::cout<<x->data<<" ";
}

void bst::postorder() {
    postorderH(root);
}
